import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DropdownModule} from 'primeng/dropdown';
import { RadioButtonModule} from 'primeng/radiobutton';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';

import { AppComponent } from './app.component';
import { FilterLevelComponent } from './filter-level/filter-level.component';
import { FilterComponent } from './filter/filter.component';
import { FilterLevelConditionComponent } from './filter-level-condition/filter-level-condition.component';

@NgModule({
  declarations: [
    AppComponent,
    FilterLevelComponent,
    FilterComponent,
    FilterLevelConditionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    DropdownModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
