import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterLevelConditionComponent } from './filter-level-condition.component';

describe('FilterLevelConditionComponent', () => {
  let component: FilterLevelConditionComponent;
  let fixture: ComponentFixture<FilterLevelConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterLevelConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterLevelConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
