import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FilterService } from '../filter.service';

@Component({
  selector: 'app-filter-level-condition',
  templateUrl: './filter-level-condition.component.html',
  styleUrls: ['./filter-level-condition.component.css']
})
export class FilterLevelConditionComponent {
  @Input() eventName: string;
  @Input() data: any;
  @Input() index: number;
  @Output() change = new EventEmitter();
  @Output() deleteCondition = new EventEmitter();

  operatorNames: {label: string, value: string}[];

  constructor(private filterService: FilterService) {
    this.operatorNames = this.filterService.getOperators().map((item) => {
      return {label: item, value: item};
    });
  }

  attributeNames(eventName): {label: string, value: string}[] {
    return this.filterService.getEventAtributes(eventName).map((item) => {
      return {label: item, value: item};
    });
  }
}
