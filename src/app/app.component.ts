import { Component } from '@angular/core';
import { FilterService } from './filter.service';
import { LanguageService } from './language.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private ls: LanguageService){
    ls.language.subscribe({
      next: (lang: string) => {console.log("AppComponent: language has changed to: " + lang)}
    });
  }
}
