import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  public language = new Observable<string>((observer) => {
    LanguageService.prototype.changeLanguage = (lang: string) => {
      observer.next(lang);
    }
  });

  constructor() {}

  public changeLanguage(lang: string) {

  }
}
