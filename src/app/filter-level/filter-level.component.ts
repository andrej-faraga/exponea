import { Component,  Input,  Output, EventEmitter, OnInit} from '@angular/core';
import { FilterService } from '../filter.service';


interface FilterLevelObject {
  eventName?: string;
  conditions?: {
    attributeName?: string;
    operator?: string;
    value?: string;
  }[];
}

@Component({
  selector: 'app-filter-level',
  templateUrl: './filter-level.component.html',
  styleUrls: ['./filter-level.component.css']
})
export class FilterLevelComponent implements OnInit {
  @Input() id: number;
  @Input() data: FilterLevelObject;

  @Output() itemDeleted = new EventEmitter();
  @Output() itemCopied = new EventEmitter();

  constructor(private filterService: FilterService) {}

  eventNames(): {label: string, value: string}[] {
    return this.filterService.getEvents().map((item) => {
      return {label: item.name, value: item.name};
    });
  }

  addCondition() {
    this.data.conditions.push({});
  }

  deleteCondition(index) {
    this.data.conditions.splice(index, 1);
  }

  truncateConditions() {
    // this.data.conditions.splice(0);
    this.data.conditions = [];
  }

  emitDelete() {
    this.itemDeleted.emit(this.id);
  }

  emitCopy() {
    this.itemCopied.emit(this.id);
  }

  ngOnInit() {
    if (typeof this.data.conditions === 'undefined') {
      this.data.conditions = [];
    }
  }
}
