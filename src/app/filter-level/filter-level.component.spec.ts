import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterLevelComponent } from './filter-level.component';

describe('FilterLevelComponent', () => {
  let component: FilterLevelComponent;
  let fixture: ComponentFixture<FilterLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
