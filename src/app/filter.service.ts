import { Injectable } from '@angular/core';


interface Event {
  name: string;
  attributes?: string[];
}


@Injectable({
  providedIn: 'root'
})
export class FilterService {

  events: Event[] = [
    {
      name: 'python_script_event',
      attributes: ['A', 'B', 'C', 'D', 'E', 'price', 'timestamp', 'index']
    },
    {
      name: 'python_script_event_2',
      attributes: ['price', 'timestamp', 'index']
    },
    {
      name: 'python_script_event_3',
      attributes: ['price', 'timestamp', 'index']
    },
    {
      name: 'python_script_event_4',
      attributes: ['price', 'timestamp', 'index']
    },
    {
      name: 'session_start',
      attributes: ['price', 'something', 'started']
    }
  ];


  operators: string[] = [
    'equals_to',
    'does_not_equal',
    'contain'
  ];

  constructor() { }

  getEvents(): Event[] {
    return this.events;
  }

  getOperators(): string[] {
    return this.operators;
  }

  getEventAtributes(evtName: string): string[] {
    return this.getEvents().filter((event) => {
      return event.name === evtName;
    })[0].attributes;
  }
}
