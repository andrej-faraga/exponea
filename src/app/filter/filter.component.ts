import { Component, OnInit, Input, AfterViewInit, ViewChild, ViewChildren, QueryList, DoCheck } from '@angular/core';
import { FilterLevelComponent } from '../filter-level/filter-level.component';
import { Subscription } from 'rxjs';
import { FilterService } from '../filter.service';
import { LanguageService } from '../language.service';
import * as _ from 'lodash';

// function clone(object): any {
//   const cloneObject = {};
//   for (const prop in object) {
//     if (object.hasOwnProperty(prop)) {
//       if (typeof object[prop] === 'object') {
//         cloneObject[prop] = clone(object[prop]);
//       } else {
//         cloneObject[prop] = object[prop];
//       }
//     }
//   }
//   return cloneObject;
// }


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements AfterViewInit, DoCheck {
  @ViewChildren(FilterLevelComponent) filterLevelComponent !: QueryList<FilterLevelComponent>;
  private subs: Subscription[] = [];
  private value = [{}];
  private dataPreview = false;
  private language = 'SK';

  constructor(private fs: FilterService, private ls: LanguageService) {}

  applyFilters(): void {
    console.log(this.value);
  }

  addFilterLevel(): void {
    this.value.push({});
  }

  truncateFilters(): void {
    this.value = [{}];
  }

  startListening(): void {
    this.filterLevelComponent.toArray().forEach((fl: FilterLevelComponent) => {
      // level value deleted
      this.subs.push(fl.itemDeleted.subscribe({next: (id) => {
        this.value.splice(id, 1);
      }}));

      // level value copied
      this.subs.push(fl.itemCopied.subscribe({next: (id) => {
        // need to perform full object clone
        this.value.push(_.cloneDeep(this.value[id]));
      }}));
    });
  }

  stopListening() {
    this.subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

  resetListening() {
    this.stopListening();
    this.startListening();
  }


  ngOnInit(): void {
    // listen to outer changes
    window.addEventListener('ng.data.in.exponea', (event: CustomEvent) => {
      console.log('ng.data.in.exponea - handler');
      this.value = event.detail;
    });

    // ask initial data from bpm
    window.dispatchEvent(new CustomEvent('ng.data.init.exponea'));
    console.log('ngOnInit');
  }


  ngAfterViewInit(): void {
    this.startListening();

    this.filterLevelComponent.changes.subscribe({
      next: () => {
        this.resetListening();
      }
    });

    // register change detection mechanism
    FilterComponent.prototype["ngDoCheck"] = () => {
      window.dispatchEvent(new CustomEvent('ng.data.out.exponea', {detail: this.value}));
      console.log('ngDoCheck');
    }
  }


  ngDoCheck(): void {
    //empty until registered in ngAfterViewInit
  }

  private changeLanguage($event) {
    this.ls.changeLanguage($event.target.value);
  }
}
