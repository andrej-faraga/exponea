var gulp = require('gulp');
var config = require('./gulp.config');
var clean = require('gulp-clean');

gulp.task('default', defaultTask);
gulp.task('clean', cleanTask);
gulp.task('copy', copyTask);
gulp.task('deploy', deployTask);


function defaultTask(done) {
  console.log("Hello Gulp");
  done();
}

// delete existing files from apache publishing directory
function cleanTask() {
  return gulp.src(config.publishingDirectory + 'exponea/', { read: false, allowEmpty: true })
    .pipe(clean({force: true}));
}

//copy built files to apache publishing directory
function copyTask() {
  return gulp.src('dist/**/*')
    .pipe(gulp.dest(config.publishingDirectory));
}

//all together
function deployTask(done) {
  gulp.series('clean', 'copy')();
  done();
};

